*** Settings ***
Resource         resources/common.resource
Suite Setup      Open browser to search country page
Test Template    Search Country page should show FR / France
Suite Teardown   Close browser

*** Test Cases ***            Country
Country France by Alpha       FR
Country France by Label       France

*** Keywords ***
Search Country page should show FR / France
    [Arguments]    ${country}
    Input Country    ${country}
    Submit Country
    Wait Until Page Contains    France