<?php


class Country
{
    /**
     * @var array
     */
    private $countries;

    public function __construct()
    {
        $this->countries = [
            'FR' => 'France',
            'BE' => 'Belgique'
            // ...
        ];
    }

    public function getCountries()
    {
        return $this->countries;
    }

    public function getCountryLabelByAlpha2($alpha)
    {
        if(empty($alpha) || !isset($this->countries[$alpha]))
        {
            return null;
        }

        return $this->countries[$alpha];
    }

    public function getCountryAlpha2ByLabel($label)
    {
        if (empty($label)) {
            return null;
        }

        foreach ($this->countries as $alpha => $clabel)
        {
            if($label === $clabel) {
                return $alpha;
            }
        }

        return null;
    }
}