<?php

include_once(__DIR__ . "/Country.php");

$countries = (new Country())->getCountries();

foreach ($countries as $alpha => $label) {
    echo sprintf('Alpha : %s | Label : %s', $alpha, $label);
}