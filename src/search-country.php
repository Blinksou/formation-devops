<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


include_once(__DIR__ . "/Country.php");

$submittedCountry = $_POST['country'] ?? null;

if($submittedCountry) {
    $country = (new Country())->getCountryLabelByAlpha2($submittedCountry) ?? (new Country())->getCountryAlpha2ByLabel($submittedCountry);

    if($country) {
        echo sprintf('Alpha : %s | Label : %s', $submittedCountry, $country);
    } else {
        echo 'Not found';
    }
}

?>

<form method="post">
    <label for="country">Country</label>
    <input name="country" id="country">

    <button type="submit" id="submit_country_search">SUBMIT</button>
</form>
