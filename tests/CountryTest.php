<?php

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../src/vendor/autoload.php'; // Autoload files using Composer autoload
include_once(__DIR__ . "/../src/Country.php");

class CountryTest extends TestCase
{
    public function testGetCountryByAlpha()
    {
        $country = new Country();
        $result = $country->getCountryLabelByAlpha2('FR');
        $expected = 'France';

        $this->assertEquals($result, $expected);
    }

    public function testGetCountryByUnknownAlpha()
    {
        $country = new Country();
        $result = $country->getCountryLabelByAlpha2('TG');

        $this->assertEmpty($result);
    }
}
