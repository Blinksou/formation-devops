<?php

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../src/vendor/autoload.php'; // Autoload files using Composer autoload
include_once(__DIR__ . "/../src/City.php");

class CityTest extends TestCase
{
    public function testgetCityNameById()
    {
        $city = new City();
        $result = $city->getCityNameById(1);
        $expected = 'Bordeaux';

        $this->assertEquals($result, $expected);
    }
}
